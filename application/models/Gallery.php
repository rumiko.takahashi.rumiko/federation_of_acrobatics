<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/15/18
 * Time: 8:37 PM
 */

namespace application\models;


use application\core\Db;
use PDO;

class Gallery
{

    const SHOW_BY_DEFAULT = 5;

    public static function getListPhoto($id){

    }


    public static function addComment($comment, $userId, $photoId, $ownerId) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO comments (id_img, id_sender, id_owner, comment_text) VALUES  (?, ?, ?, ?)';

        $result = $db->prepare($sql);

        $result->execute(array($photoId, $userId, $ownerId, $comment));

    }

    public static function addPhoto($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO images  (owner_id, name_img, description_img) VALUES  (?, ?, ?)';

        $result = $db->prepare($sql);
        $result = $result->execute(array($options['owner_id'], $options['name_img'],  $options['description']));

        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getMaxId() {
        $db = Db::getConnection();

        $sql = 'select MAX(id) from images;';

        $result = $db->prepare($sql);

        $result->execute();
        return $result->fetch();
    }

    public static function getOwnerPhoto($id) {
        $db = Db::getConnection();

        $sql = 'select owner_id from images where id='.$id;

        $result = $db->prepare($sql);
        $result->execute();
        $ownerId = $result->fetch();

        return $ownerId['owner_id'];
    }

    public static function getAllComment($photoId) {
        $db = Db::getConnection();

        $sql = 'SELECT id_img, id_sender, id_owner, comment_text, comment_data FROM comments WHERE id_img = ?'
        .'ORDER BY comment_data DESC;';

        $result = $db->prepare($sql);
        $result->execute(array($photoId));

        $i = 0;
        $commentList = array();
        while ($row = $result->fetch()){
            $commentList[$i]['name_sender'] = User::getNameFromId($row['id_sender']);
            $commentList[$i]['name_owner'] = User::getNameFromId($row['id_owner']);
            $commentList[$i]['comment_text'] = $row['comment_text'];
            $commentList[$i]['comment_data'] = $row['comment_data'];
            $i++;

        }
        return $commentList;
    }

    public static function getAllPhoto($routs) {
        $max = 5;

        if (isset($routs['page'])){
            $start = ($routs['page'] - 1) * $max;
        } else {
            $start = 1;
        }



        $db = Db::getConnection();

        $start = intval($start);
        $max = intval($max);

        $sql = 'SELECT * FROM images ORDER BY id DESC LIMIT '.$start.','.$max.';';



        $result = $db->prepare($sql);
        $result->execute();

        $i = 0;
        $imageList = array();
        while ($row = $result->fetch()){
            $imageList[$i]['id'] = $row['id'];
            $imageList[$i]['owner_id'] = $row['owner_id'];
            $imageList[$i]['name_img'] = $row['name_img'];
            $imageList[$i]['description_img'] = $row['description_img'];
            $i++;
        }

        return $imageList;
    }

}
