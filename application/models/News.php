<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 12/20/18
 * Time: 8:28 PM
 */

namespace application\models;


use application\core\ConnectDb;
use \PDO;

class News
{
    const SHOW_BY_DEFAULT = 5;

    public static function getLastId(){
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT MAX(ID) FROM news';

        $result = $db->prepare($sql);
        $result->execute();
        $id = $result->fetchColumn();
        return $id;
    }

    public static function addNews($option){
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'INSERT INTO news (titel, description, text_news, photo) VALUES (?, ?, ?, ?)';

        $result = $db->prepare($sql);
        $result = $result->execute(array($option['title'], $option['description'], $option['text_news'], $option['photo']));

        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getAllNews($routs) {
        $max = 5;

        if (isset($routs['id'])){
            $start = ($routs['id'] - 1) * $max;
        } else {
            $start = 1;
        }

        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT * FROM news ORDER BY id DESC LIMIT '.$start.','.$max.';';
        
        $result = $db->prepare($sql);
        $result->execute();

        $i = 0;
        $news = array();
        while ($row = $result->fetch()){
            $news[$i]['id'] = $row['id'];
            $news[$i]['titel'] = $row['titel'];
            $news[$i]['description'] = $row['description'];
            $news[$i]['text_news'] = $row['text_news'];
            $news[$i]['photo'] = $row['photo'];
            $news[$i]['date'] = $row['date'];
            $i++;
        }
        return $news;
    }

    public static function getOneNews($trenerId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "SELECT * FROM news WHERE id=:user_id";

        $result = $db->prepare($sql);
        $result->execute(['user_id' => $trenerId]);
        $trener = $result->fetch(PDO::FETCH_ASSOC);
        return $trener;
    }

    public static function getAllInformationNews() {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT * FROM news';

        $result = $db->prepare($sql);
        $result->execute();

        $i = 0;
        $news = array();
        while ($row = $result->fetch()){
            $news[$i]['id'] = $row['id'];
            $news[$i]['titel'] = $row['titel'];
            $news[$i]['description'] = $row['description'];
            $news[$i]['text_news'] = $row['text_news'];
            $news[$i]['photo'] = $row['photo'];
            $news[$i]['date'] = $row['date'];
            $i++;
        }
        return $news;
    }

    public static function deleteNews($id) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "DELETE FROM news WHERE id=:id";

        $result = $db->prepare($sql);
        return  $result->execute(['id' => $id]);
    }

    public static function updateNews($option, $id) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'UPDATE news SET titel=?, description=?, text_news=?, photo=? WHERE id=?';

        $result = $db->prepare($sql);
        $result = $result->execute(array($option['title'], $option['description'], $option['text_news'], $option['photo'], $id));
        return $result;

    }
}