<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 1/11/19
 * Time: 11:35 PM
 */

namespace application\controller;


use application\core\Controller;
use application\core\PaginationTrener;
use application\models\News;
use application\models\User;

class TrenirController extends Controller
{
    public function indexAction() {
        $trenera = User::getAllTrener($this->route);

        $lastId = User::getLastIdTrener();

        $pagination = new PaginationTrener($this->route, $lastId);

        $vars = [
            'trenera' => $trenera,
            'pagination' => $pagination->get(),
        ];


        $this->view->render('ФСАУ | Тренера', $vars);
        return true;
    }

    public function trenirAction() {

        $trenitId = $this->route['id'];

        $information = User::getInformationTrener($trenitId);

        $vars = $information;

        $this->view->render('ФСАУ | Тренера', $vars);
        return true;
    }
}