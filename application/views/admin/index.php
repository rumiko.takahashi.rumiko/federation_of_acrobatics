<section>
    <div class="container">
        <div class="row">

            <br/>

            <h4>Добрый день, администратор!</h4>

            <br/>

            <p>Вам доступны такие возможности:</p>

            <br/>

            <ul>
                <li><a href="/admin/news">Управління новинами</a></li>
                <li><a href="/admin/events">Управління заходами</a></li>
                <li><a href="/admin/trener">Додати тренера</a></li>
            </ul>

        </div>
    </div>
</section>
