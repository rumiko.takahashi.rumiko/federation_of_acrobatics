<section>
    <div class="container">
        <div class="row">

            <br/>

            <h4>Редагувати новину</h4>

            <br/>

            <div class="col-lg-12">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">

                        <p>Название новини</p>
                        <input type="text" name="name" placeholder="" value="<?php echo $vars['titel']; ?>">

                        <br/><br/>

                        <p>Описання</p>
                        <textarea name="description"><?php echo $vars['description']; ?></textarea>

                        <br/><br/>

                        <p>Текст новини</p>
                        <textarea cols="30" rows="7" name="text_news"><?php echo $vars['text_news']; ?></textarea>


                        <br/><br/>

                        <p>Зображення новини</p>
                        <img src="../public/images/news/<?php echo $vars['photo'];?>.jpg" width="200" alt="" />
                        <input type="file" name="image" placeholder="" value="../public/images/news/<?php echo $vars['photo']; ?>.jpg">

                        <br/><br/>

                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">

                        <br/><br/>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>
