<section>
    <div class="container">
        <div class="row">

            <br/>

            <h4>Добавити новину</h4>

            <br/>


            <div class="col-lg-10">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">
                        <p>Заголовок новини</p>
                        <input type="text" name="name" placeholder="" value="">
                        <br/><br/>
                        <p>Описання</p>
                        <textarea name="description"></textarea>
                        <br/><br/>
                        <p>Текст новини</p>
                        <textarea name="text_news"></textarea>
                        <br/><br/>
                        <p>Фото обложки</p>
                        <input type="file" name="image" placeholder="" value="" class="col-lg-6">
                        <br/><br/>
                        <input type="submit" name="submit" class="btn btn-default" value="Добавити">
                        <br/><br/>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">

            <h4>Список товаров</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID новини</th>
                    <th>Назва новини</th>
                    <th>Описання</th>
                    <th>Текст новини</th>
                    <th>Редагувати</th>
                    <th>Удалити</th>
                </tr>
                <?php foreach ($vars as $news): ?>
                    <tr>
                        <td><?php echo $news['id']; ?></td>
                        <td><?php echo $news['titel']; ?></td>
                        <td><?php echo $news['description']; ?></td>
                        <td><?php echo $news['text_news']; ?></td>
                        <td><a href="/newsupdate/<?php echo $news['id'];?>" title="Редагувати"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/newsdelete/<?php echo $news['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>
</section>
