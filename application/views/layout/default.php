<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $title; ?></title>
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/css/font-awesome.min.css" rel="stylesheet">
    <link href="../public/css/prettyPhoto.css" rel="stylesheet">
    <link href="../public/css/price-range.css" rel="stylesheet">
    <link href="../public/css/animate.css" rel="stylesheet">
    <link href="../public/css/main.css" rel="stylesheet">
    <link href="../public/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../public/js/html5shiv.js"></script>
    <script src="../public/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../public/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../public/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../public/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../public/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <p>Федерація Спортивної Акробатики України</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">

                            <li class="dropdown"><a href="/">Федерація<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="/kerivnitstvo">Керівництво</a></li>
                                    <li><a href="/trenir/1">Тренерський склад</a></li>
                                    <li><a href="/documentation">Документація</a></li>
                                    <li><a href="/partner">Партнери</a></li>
                                </ul>
                            </li>

                            <li><a href="/events">Змагання</a></li>
                            <li><a href="/uspihi">Успіхи</a></li>
                            <li><a href="/zbirna">Збірна</a></li>
                            <li><a href="/vitany">Вітання</a></li>
                            <li><a href="/news/1">Новини</a></li>
                            <li><a href="/contact">Контакти</a></li>

                            <li class="dropdown"><a href="/gallery">Галерея<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="/gallery/photo">Фото</a></li>
                                    <li><a href="/gallery/video">Відео</a></li>
                                </ul>
                            </li>

<!--                            for admin panel-->
                            <li class="dropdown"><a href="/admin">Адміністратор<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="/admin/news">Редагувати новини</a></li>
                                    <li><a href="/admin/trener">Редагувати список тренерів</a></li>
                                    <li><a href="/admin/events">Редагувати події</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

<?php echo $content;?>

<footer id="footer"><!--Footer-->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2018 <a href="https://www.facebook.com/profile.php?id=100004512672168">vbudnik</a></p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->



<script src="../public/js/jquery.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
<script src="../public/js/jquery.scrollUp.min.js"></script>
<script src="../public/js/price-range.js"></script>
<script src="../public/js/jquery.prettyPhoto.js"></script>
<script src="../public/js/main.js"></script>
</body>
</html>