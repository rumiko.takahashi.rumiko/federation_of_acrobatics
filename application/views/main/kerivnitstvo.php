<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-center">

                <div class="product-details"><!--product-details-->
                <div class="col-sm-4">
                    <div class="view-product">
                        <img src="../public/images/product-details/1.jpg" alt="" />
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="product-information"><!--/product-information-->
                        <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                        <h2>Comander Comander</h2>
                        <img src="images/product-details/rating.png" alt="" />
                        <p><b>Contact:</b> tel...</p>
                        <p><b>Other information:</b> information</p>
                    </div><!--/product-information-->
                </div>
            </div><!--/product-details-->
                <div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tsil" data-toggle="tab">Інформація</a></li>
                            <li><a href="#blazers" data-toggle="tab">Історія</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tsil" >
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="productinfo text-left">
                                        <p>Батьки повинні чітко розуміти, яких результатів очікують від дитини, коли приводять її на акробатику. Як вид спорту, вона дуже складна для дитини, до того ж дуже унікальна. Медики відзначають, що тренування з акробатики включають відразу кілька рішень для зростаючого дитячого організму. У маленького акробата буде прекрасна фізична підготовка, при якій навантаження розподіляється так, щоб задіяти всі групи м'язів.</p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="blazers" >
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <p>Среди самых больших заслуг Вики —  серебро и две бронзы Универсиады в Казани в 2013 году (групповые упражнения), серебро и бронза Чемпионатов Европы 2013 и 2015 (команда), а также 4 бронзы с Чемпионатов мира: 2011 — командные соревнования, 2013 – групповое первенство, 2014 и 2015 — командные соревнования.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/category-tab-->
            </div>
        </div>
    </div>
</section>

